import * as expect from "expect";
import { ContentArgs, ContentData, EditionArgs, EditionData, PublicationArgs } from "@mfgames-writing/contracts";
import "mocha";
import { GuillemetPlugin } from "../guillemet";

describe("parsing", function()
{
    it("single pair", function(done)
    {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "<<hiya>>";

        // Create the plugin and process data using it.
        const plugin = new GuillemetPlugin();
        let promise = plugin.process(contentArgs);

        promise.then(c =>
        {
            expect(c.text).toEqual("«hiya»");
            done();
        });
    });

    it("multiple pair", function(done)
    {
        // Create the input we'll be processing.
        let publicationArgs = new PublicationArgs(null, null, null);
        let editionData = {} as EditionData;
        let editionArgs = new EditionArgs(publicationArgs, "test", editionData);
        let contentData = {} as ContentData;
        let contentArgs = new ContentArgs(editionArgs, contentData);

        contentArgs.text = "<<hiya,>> he said, <<why?>>";

        // Create the plugin and process data using it.
        const plugin = new GuillemetPlugin();
        let promise = plugin.process(contentArgs);

        promise.then(c =>
        {
            expect(c.text).toEqual("«hiya,» he said, «why?»");
            done();
        });
    });
});
